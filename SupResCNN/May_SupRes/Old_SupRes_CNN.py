#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import sys
import random
import numpy as np
import h5py
import matplotlib.pyplot as plt
import math
import uproot 
import time
from training_func import*
from matplotlib.colors import LogNorm


# In[2]:


NEvent = 90000
n_epochs = 200
NAME = "_SupRes_ChNu"
PATH_NN='model_NN'+str(NAME)+'.pt'


# In[3]:


import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
from torch import tensor
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor


# In[4]:


true_pix, orig_pix, det_size = 128, 64, 125.
LayerPix = np.array([64, 16])
supL=np.array([64, 64])
No_Lay=np.array([13,34])


# In[5]:


train_Layer=np.load("numpy_fil/train_Layer.npy")
val_Layer=np.load("numpy_fil/val_Layer.npy")
train_ch=np.load("numpy_fil/train_ch.npy")
train_nu=np.load("numpy_fil/train_nu.npy")
val_ch=np.load("numpy_fil/val_ch.npy")
val_nu=np.load("numpy_fil/val_nu.npy")


# In[6]:


print("Numpy Imported,NEvent - ",val_Layer.shape[0]+train_Layer.shape[0])


# In[7]:


ev, l = 23, 1
print(LayerPix)
plt.figure(figsize=(11, 11))

plt.subplot(221)
plt.title("INPUT NN")
plt.imshow(val_Layer[ev][l][:LayerPix[l],:LayerPix[l]].T, cmap = "cividis")
plt.colorbar()

plt.subplot(222)
plt.title("CH TAR")
plt.imshow(val_ch[ev][l].T, cmap="hot")
plt.colorbar()

plt.subplot(223)
plt.title("NU TAR")
plt.imshow(val_nu[ev][l].T, cmap="GnBu_r")
plt.colorbar()

plt.subplot(224)
plt.title("NU+CH TAR")
plt.imshow(val_nu[ev][l].T+val_ch[ev][l].T, cmap="YlGn_r")
plt.colorbar()

#plt.savefig("preprocess_fig.png")
#print("Saved Preprocess image")

# In[8]:


os.environ["CUDA_VISIBLE_DEVICES"]="0"


# In[9]:


cuda_device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu' )


# In[10]:


torch.cuda.get_device_name( torch.cuda.current_device() )


# In[11]:


###############

#CNN 

##############

class Upconv(nn.Module):
    def __init__(self, upscale_factor):
        super(Upconv, self).__init__()

        self.prelu = nn.LeakyReLU(negative_slope=0.1)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm2d(16)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm2d(128)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(256)
        
        self.conv1 = nn.Conv2d(1, 16,kernel_size=3,stride=1,padding=1)
        self.conv2 = nn.Conv2d(16, 32,kernel_size=3,stride=1,padding=1)
        self.conv3 = nn.Conv2d(32, 64,kernel_size=3,stride=1,padding=1) 
        self.conv4 = nn.Conv2d(64, 128,kernel_size=3,stride=1,padding=1) 
        self.conv5 = nn.Conv2d(128, 128,kernel_size=3,stride=1,padding=1)
        self.conv6 = nn.Conv2d(128, 256,kernel_size=3,stride=1,padding=1)
        #self.conv7 = nn.ConvTranspose2d(256, 1,kernel_size=4,stride=2,padding=1,bias=False)
        self.conv8 = nn.PixelShuffle(upscale_factor)
        self.convf = nn.Conv2d( int(256/upscale_factor**2), 1,kernel_size=1)

    def forward(self, x):
        x = self.bn1(self.prelu(self.conv1(x)))
        x = self.bn2(self.prelu(self.conv2(x)))
        x = self.bn3(self.prelu(self.conv3(x)))
        x = self.bn4(self.prelu(self.conv4(x)))
        x = self.bn5(self.prelu(self.conv5(x)))
        x = self.bn6(self.prelu(self.conv6(x)))
        x = self.conv8(x)
        x = self.convf(x)
        x = self.relu(x)
        
        
        return x


# In[16]:


class SuperResModel(nn.Module):
    
    def __init__(self):
        super(SuperResModel, self).__init__()
        self.cnn1 = Upconv(upscale_factor = int(orig_pix/LayerPix[0]) ).float()
        self.cnn2 = Upconv(upscale_factor = int(orig_pix/LayerPix[1]) ).float()


        self.cnn_comb = nn.Conv2d(3, 4,kernel_size=1,stride=1,padding=0)
        
        self.cnn1_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
         
        self.cnn1_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
        
        #self.cnn1_out_tot = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        #self.cnn2_out_tot = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)

    def forward(self, x):
        

        #out1 = self.cnn1( x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float() )
        out1 = x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float()
        out2 = self.cnn2( x[:,1:2, 0:LayerPix[1], 0:LayerPix[1]].float() )
        out3 = x[:, 2:3, 0:orig_pix, 0:orig_pix].float()
        
        merged_im = torch.cat(  (out1, out2, out3), dim=1 )
        
        out_int = self.cnn_comb(merged_im)
        
        out1_f_ch = ExpandLayer(  self.cnn1_out_ch(out_int[:, 0:1, :, :])  )
        out2_f_ch = ExpandLayer(  self.cnn2_out_ch(out_int[:, 1:2, :, :])  )
        
        out1_f_nu = ExpandLayer(  self.cnn1_out_nu(out_int[:, 2:3, :, :])  )
        out2_f_nu = ExpandLayer(  self.cnn2_out_nu(out_int[:, 3:4, :, :])  )
        
        #out1_f_tot = ExpandLayer(  self.cnn1_out_tot(out_int[:, 0:1, :, :])  )
        #out2_f_tot = ExpandLayer(  self.cnn2_out_tot(out_int[:, 1:2, :, :])  )
        
        merged =  torch.cat(  (out1_f_ch, out2_f_ch, 
                               out1_f_nu, out2_f_nu), dim=1 )
        #merged =  torch.cat(  (out1_f_tot, out2_f_tot, 
        #                       out1_f_nu, out2_f_nu), dim=1 )
        
        
        return merged


# In[17]:


model = SuperResModel()
model.to(cuda_device)


# In[18]:


test_out = model(  Tensor(val_Layer[23:24]).cuda() )

# In[19]:

print("TEST OUT SHAPE:",test_out.shape)

# In[21]:


train_dataset = TensorDataset( Tensor( torch.from_numpy(train_Layer).float() ),
                               Tensor( torch.from_numpy(  train_ch ).float() ),
                               Tensor( torch.from_numpy(  train_nu ).float() )  )
valid_dataset = TensorDataset( Tensor( torch.from_numpy(  val_Layer).float() ),
                               Tensor( torch.from_numpy( val_ch    ).float() ),
                               Tensor( torch.from_numpy( val_nu    ).float() )  )

#train_dataset = TensorDataset( Tensor( torch.from_numpy(train_Layer).float() ),
#                               Tensor( torch.from_numpy(  train_ch+train_nu ).float() ),
#                               Tensor( torch.from_numpy(  train_nu ).float() )  )
#valid_dataset = TensorDataset( Tensor( torch.from_numpy(  val_Layer).float() ),
#                               Tensor( torch.from_numpy( val_ch+val_nu    ).float() ),
#                               Tensor( torch.from_numpy( val_nu    ).float() )  )


# In[22]:


train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=30)
valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=30)


# In[23]:


optimizer = optim.Adam(model.parameters(), lr=0.0001)
total_step = len(train_loader)
print(total_step)


# In[25]:



###---- LOSS FUNCTION OF CNN ----###
def LossFunction(pred,tar_ch ,tar_nu ) : 
    pred_ch=pred[:,0:2,:,:]
    pred_nu=pred[:,2:4,:,:]
    
    wt_nu=torch.sum( ( tar_nu.cuda() - pred_nu.cuda() )*( tar_nu.cuda() - pred_nu.cuda() ) )
    wt_nu/=torch.sum(tar_nu.cuda())
    wt_ch=torch.sum( ( tar_ch.cuda() - pred_ch.cuda() )*( tar_ch.cuda() - pred_ch.cuda() ) )
    wt_ch/=torch.sum(tar_ch.cuda())

    
    return wt_nu+wt_ch


# In[27]:


train_loss_v, valid_loss_v = [], []

valid_loss_min = np.Inf # track change in validation loss


# In[28]:


if( len(valid_loss_v) > 0 ) : 
    valid_loss_min = np.min( np.array(valid_loss_v) )


# In[29]:


for epoch in range(1, n_epochs+1):

    # keep track of training and validation loss
    train_loss = 0.0
    valid_loss = 0.0
    
    ###################
    # train the model #
    ###################
    
    model.train() ## --- set the model to train mode -- ##
    for in_data, target_ch, target_nu in train_loader:
        # move tensors to GPU if CUDA is available
        if torch.cuda.is_available():
            #print(target.shape)
            
            in_data, target_ch, target_nu = in_data.cuda(), target_ch.cuda(), target_nu.cuda()
        # clear the gradients of all optimized variables
        optimizer.zero_grad()
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(in_data)
        # calculate the batch loss
        loss = LossFunction(output, target_ch, target_nu)
        # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()
        # perform a single optimization step (parameter update)
        optimizer.step()
        # update training loss
        train_loss += loss.item()#*data.size(0)
    
    ######################    
    # validate the model #
    ######################

    model.eval()
    for in_data, target_ch, target_nu in valid_loader:
        # move tensors to GPU if CUDA is available
        if torch.cuda.is_available():
            in_data, target_ch, target_nu = in_data.cuda(), target_ch.cuda(), target_nu.cuda()
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(in_data)
        # calculate the batch loss
        loss = LossFunction(output, target_ch, target_nu)
        # update average validation loss 
        valid_loss += loss.item()#*data.size(0)
    
    # calculate average losses
    train_loss = train_loss/len(train_loader.dataset)
    valid_loss = valid_loss/len(valid_loader.dataset)
        
        
    # print training/validation statistics 
    print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
        epoch, train_loss, valid_loss))
    
    train_loss_v.append(train_loss) 
    valid_loss_v.append(valid_loss)
    
    # save model if validation loss has decreased
    if valid_loss <= valid_loss_min:
        print('Validation loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
        valid_loss_min,
        valid_loss))
        torch.save(model.state_dict(), PATH_NN)
        valid_loss_min = valid_loss

print("... End Training!")





# In[26]:


#print(test_out.shape)



# In[31]:

#model.load_state_dict(torch.load(PATH_NN))
#model.to(cuda_device)
model.eval()

ev=20


test_out = model(  Tensor(val_Layer[ev:ev+1]).cuda() )

for l in range(2):

    plt.figure(figsize=(24, 10))

    plt.suptitle('Layer: '+str(l+1), fontsize=20)


    plt.subplot(241)
    plt.title("INPUT NN")
    plt.imshow(val_Layer[ev:ev+1][0][l][:LayerPix[l],:LayerPix[l]].T, cmap = "cividis")
    plt.colorbar()

    plt.subplot(242)
    plt.title("NU+CH TAR")
    plt.imshow(val_nu[ev][l].T+val_ch[ev][l].T, cmap="cividis")
    plt.colorbar()

    plt.subplot(245)
    plt.title("TRACK FIRST LAYER")
    plt.imshow(val_Layer[ev][2].T, cmap="hot")
    plt.colorbar()

    plt.subplot(243)
    plt.title("TARGET NU")
    plt.imshow(val_nu[ev][l].T, cmap="GnBu_r")
    plt.colorbar()

    plt.subplot(244)
    plt.title("TARGET CH")
    plt.imshow(val_ch[ev][l].T, cmap="hot")
    plt.colorbar()

    plt.subplot(247)
    plt.title("OUTPUT NN NU")
    plt.imshow(test_out[0][l+2].detach().numpy().T, cmap="GnBu_r")
    plt.colorbar()

    plt.subplot(246)
    plt.title("OUTPUT NN TOT = Ch + Nu")
    plt.imshow(test_out[0][l].detach().numpy().T+test_out[0][l+2].detach().numpy().T, cmap="cividis")
    plt.colorbar()

    plt.subplot(248)
    plt.title("OUTPUT NN CH")
    plt.imshow(test_out[0][l].detach().numpy().T, cmap="hot")
    plt.colorbar()

    plt.savefig("OUTPUT_NN"+str(l+1)+"_train.png")
    


