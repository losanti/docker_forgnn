import os
import sys
import random
import numpy as np
import h5py
import matplotlib.pyplot as plt
import math
import uproot 
import time
from training_func import*
from matplotlib.colors import LogNorm


# In[2]:


#NEvent = 10000
NAME = "_SupRes_ChNu"
PATH_NN='models/model_NN'+str(NAME)+'.pt'


# In[3]:


import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
from torch import tensor
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor


# In[4]:


true_pix, orig_pix, det_size = 128, 64, 125.
LayerPix = np.array([64, 16])
supL=np.array([64, 64])
No_Lay=np.array([13,34])


# In[5]:


#train_Layer=np.load("numpy_fil/train_Layer.npy")
val_Layer=np.load("numpy_fil/val_Layer.npy")
#train_ch=np.load("numpy_fil/train_ch.npy")
#train_nu=np.load("numpy_fil/train_nu.npy")
val_ch=np.load("numpy_fil/val_ch.npy")
val_nu=np.load("numpy_fil/val_nu.npy")


# In[6]:


print("Numpy Imported,NEvent - ",val_Layer.shape[0])

NEvent = val_Layer.shape[0]

# In[8]:


os.environ["CUDA_VISIBLE_DEVICES"]="0"


# In[9]:


cuda_device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu' )


# In[10]:


torch.cuda.get_device_name( torch.cuda.current_device() )


# In[11]:


###############

#CNN 

##############

class Upconv(nn.Module):
    def __init__(self, upscale_factor):
        super(Upconv, self).__init__()

        self.prelu = nn.LeakyReLU(negative_slope=0.1)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm2d(16)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm2d(128)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(256)
        
        self.conv1 = nn.Conv2d(1, 16,kernel_size=3,stride=1,padding=1)
        self.conv2 = nn.Conv2d(16, 32,kernel_size=3,stride=1,padding=1)
        self.conv3 = nn.Conv2d(32, 64,kernel_size=3,stride=1,padding=1) 
        self.conv4 = nn.Conv2d(64, 128,kernel_size=3,stride=1,padding=1) 
        self.conv5 = nn.Conv2d(128, 128,kernel_size=3,stride=1,padding=1)
        self.conv6 = nn.Conv2d(128, 256,kernel_size=3,stride=1,padding=1)
        #self.conv7 = nn.ConvTranspose2d(256, 1,kernel_size=4,stride=2,padding=1,bias=False)
        self.conv8 = nn.PixelShuffle(upscale_factor)
        self.convf = nn.Conv2d( int(256/upscale_factor**2), 1,kernel_size=1)

    def forward(self, x):
        x = self.bn1(self.prelu(self.conv1(x)))
        x = self.bn2(self.prelu(self.conv2(x)))
        x = self.bn3(self.prelu(self.conv3(x)))
        x = self.bn4(self.prelu(self.conv4(x)))
        x = self.bn5(self.prelu(self.conv5(x)))
        x = self.bn6(self.prelu(self.conv6(x)))
        x = self.conv8(x)
        x = self.convf(x)
        x = self.relu(x)
        
        
        return x


# In[16]:


class SuperResModel(nn.Module):
    
    def __init__(self):
        super(SuperResModel, self).__init__()
        self.cnn1 = Upconv(upscale_factor = int(orig_pix/LayerPix[0]) ).float()
        self.cnn2 = Upconv(upscale_factor = int(orig_pix/LayerPix[1]) ).float()


        self.cnn_comb = nn.Conv2d(3, 4,kernel_size=1,stride=1,padding=0)
        
        self.cnn1_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
         
        self.cnn1_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
        
        #self.cnn1_out_tot = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        #self.cnn2_out_tot = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)

    def forward(self, x):
        

        out1 = self.cnn1( x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float() )
        #out1 = x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float()
        out2 = self.cnn2( x[:,1:2, 0:LayerPix[1], 0:LayerPix[1]].float() )
        out3 = x[:, 2:3, 0:orig_pix, 0:orig_pix].float()
        
        merged_im = torch.cat(  (out1, out2, out3), dim=1 )
        
        out_int = self.cnn_comb(merged_im)
        
        out1_f_ch = ExpandLayer(  self.cnn1_out_ch(out_int[:, 0:1, :, :])  )
        out2_f_ch = ExpandLayer(  self.cnn2_out_ch(out_int[:, 1:2, :, :])  )
        
        out1_f_nu = ExpandLayer(  self.cnn1_out_nu(out_int[:, 2:3, :, :])  )
        out2_f_nu = ExpandLayer(  self.cnn2_out_nu(out_int[:, 3:4, :, :])  )
        
        #out1_f_tot = ExpandLayer(  self.cnn1_out_tot(out_int[:, 0:1, :, :])  )
        #out2_f_tot = ExpandLayer(  self.cnn2_out_tot(out_int[:, 1:2, :, :])  )
        
        merged =  torch.cat(  (out1_f_ch, out2_f_ch, 
                               out1_f_nu, out2_f_nu), dim=1 )
        #merged =  torch.cat(  (out1_f_tot, out2_f_tot, 
        #                       out1_f_nu, out2_f_nu), dim=1 )
        
        
        return merged


# In[17]:

model = SuperResModel()

model.load_state_dict(torch.load(PATH_NN))
model.to(cuda_device)


test_dataset = TensorDataset( Tensor( torch.from_numpy(  val_Layer).float() )  )


# In[22]:


test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1)



Nu_Pred=np.zeros((NEvent,2,orig_pix,orig_pix))
Ch_Pred=np.zeros((NEvent,2,orig_pix,orig_pix))


i=0
for  data_in  in test_loader:
    #if i>0: break
    
    if torch.cuda.is_available():
        #print(data_in[0].shape)
        data_in = data_in[0].cuda()
    #print(data_in.shape)
    model.eval()
    output=model(data_in)
    #print(output.shape)
    Ch_Pred[i]= output.detach()[0][0:2]
    Nu_Pred[i] = output.detach()[0][2:4]

    if i%200==0:print(i, output.detach().shape )
    i+=1


hf = h5py.File('output/ML_SupRes.h5','w')
hf.create_dataset('ML_Ch_SupRes', data=Ch_Pred)
hf.create_dataset('ML_Nu_SupRes', data=Nu_Pred)

#hf.create_dataset('Charged_Sres', data=val_ch)
#hf.create_dataset('Neutral_Sres', data=val_nu)

#hf.create_dataset('Total_Input', data=val_Layer)
#hf.create_dataset('Neutral_Rres', data=Nu_in)

hf.close()




# In[31]:

ev=20


test_out = model(  Tensor(val_Layer[ev:ev+1]).cuda() )


for l in range(2):

    plt.figure(figsize=(24, 10))

    plt.suptitle('Layer: '+str(l+1), fontsize=20)


    plt.subplot(241)
    plt.title("INPUT NN")
    plt.imshow(val_Layer[ev:ev+1][0][l][:LayerPix[l],:LayerPix[l]].T, cmap = "cividis")
    plt.colorbar()
    
    plt.subplot(242)
    plt.title("NU+CH TAR")
    plt.imshow(val_nu[ev][l].T+val_ch[ev][l].T, cmap="cividis")
    plt.colorbar()

    plt.subplot(245)
    plt.title("TRACK FIRST LAYER")
    plt.imshow(val_Layer[ev][2].T, cmap="hot")
    plt.colorbar()
    
    plt.subplot(243)
    plt.title("TARGET NU")
    plt.imshow(val_nu[ev][l].T, cmap="GnBu_r")
    plt.colorbar()
    
    plt.subplot(244)
    plt.title("TARGET CH")
    plt.imshow(val_ch[ev][l].T, cmap="hot")
    plt.colorbar()
    
    plt.subplot(247)
    plt.title("OUTPUT NN NU")
    plt.imshow(test_out[0][l+2].detach().numpy().T, cmap="GnBu_r")
    plt.colorbar()
    
    plt.subplot(248)
    plt.title("OUTPUT NN CH")
    plt.imshow(test_out[0][l].detach().numpy().T, cmap="hot")
    plt.colorbar()

    plt.subplot(246)
    plt.title("OUTPUT NN TOT=CH+NU")
    plt.imshow(test_out[0][l].detach().numpy().T+test_out[0][l+2].detach().numpy().T, cmap="cividis")
    plt.colorbar()

    
    plt.savefig("OUTPUT_NN"+str(l+1)+"_test.png")
    


