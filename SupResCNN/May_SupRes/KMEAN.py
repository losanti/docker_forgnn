#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
#import seaborn as sns; sns.set()  # for plot styling
from sklearn.cluster import KMeans
from pandas import DataFrame
import random
from sklearn.datasets.samples_generator import make_blobs
from scipy.stats import skewnorm
import uproot
import time
import math as m
import h5py

# In[2]:

f_ML = h5py.File("output/ML_SupRes.h5","r")
f_input = h5py.File("output/true_input.h5","r")


print(f_ML.keys())
print(f_input.keys())

#f_ML[""
#f = uproot.open('/home/santilor/GPU_SupRes/PFlowNtupleFile_HOMDet_2to5GeV_Overlap_WS.root')
#f['EventTree'].keys()


# In[3]:


orig_pix, det_size = 64, 125.
LayerPix = np.array([64, 16])
supL = np.array([64, 64])

NEvent = 1000

#dim in cm 
X_0=3.9
l_I=17.4
orig=150
length=np.array([3*X_0,16*X_0,6*X_0,1.5*l_I,4.1*l_I,1.8*l_I])
print("Layers depth: ",length)

orig_b=150

Z=np.array([l/2. for l in length])
Z=orig_b+np.array([Z[0], 2*Z[0]+Z[1], 2*(Z[0]+Z[1])+Z[2], 2*(Z[0]+Z[1]+Z[2])+1+Z[3], 2*(Z[0]+Z[1]+Z[2])+1+2*Z[3]+Z[4], 2*(Z[0]+Z[1]+Z[2])+1+2*(Z[3]+Z[4])+Z[5]])
print("Layer Z position: ",Z[1])



Ev_X_or = f_input['Ev_X_or']
Ev_Y_or = f_input['Ev_Y_or']


def Make_Pi0_Pos(Z_pos):
    x_cm = Ev_X_or[:] + Z_pos*np.tan(Pi0_theta[:])*np.cos(Pi0_phi[:])
    y_cm = Ev_Y_or[:] + Z_pos*np.tan(Pi0_theta[:])*np.sin(Pi0_phi[:])
    return x_cm, y_cm


# In[10]:


def Make_Trk_Pos(Z_pos):
    x_cm = Ev_X_or[:] + zpos*np.tan(Trk_theta[:])*np.cos(Trk_phi[:])
    y_cm = Ev_Y_or[:] + zpos*np.tan(Trk_theta[:])*np.sin(Trk_phi[:])
    return x_cm, y_cm


# In[11]:


def conv_to_pix(cm_array, pix):
    out = (cm_array+det_size/2.)*pix/det_size
    return out


# In[12]:


def conv_to_cm(ar, pix):
    out = (ar-pix/2.)*det_size/pix
    return out


def invmass(pmu):
    a = pmu[0]**2-pmu[1]**2-pmu[2]**2-pmu[3]**2
    if a<0:
        return -1000

    if a>=0:
        return m.sqrt(a)

ev=20
l=0


plt.figure(figsize=(30,10))

plt.subplot(131)
plt.title("ML Pred")
plt.imshow(f_ML["ML_Nu_SupRes"][ev][l][:supL[l],:supL[l]], norm=LogNorm())
plt.colorbar()


plt.subplot(132)
plt.title("Sup Res")
plt.imshow(f_input["Nu_SupRes_Target"][ev][l][:supL[l],:supL[l]], norm=LogNorm())
plt.colorbar()

plt.subplot(133)
plt.title("Real Res")
plt.imshow(f_input["Nu_RealRes_Target"][ev][l][:LayerPix[l],:LayerPix[l]])
plt.colorbar()

plt.savefig("plt/ima0.png")
plt.close()

l=1


plt.figure(figsize=(30,10))

plt.subplot(131)
plt.title("ML Pred")
plt.imshow(f_ML["ML_Nu_SupRes"][ev][l][:supL[l],:supL[l]], norm=LogNorm())
plt.colorbar()


plt.subplot(132)
plt.title("Sup Res")
plt.imshow(f_input["Nu_SupRes_Target"][ev][l][:supL[l],:supL[l]], norm=LogNorm())
plt.colorbar()

plt.subplot(133)
plt.title("Real Res")
plt.imshow(f_input["Nu_RealRes_Target"][ev][l][:LayerPix[l],:LayerPix[l]])
plt.colorbar()

plt.savefig("plt/ima1.png")
plt.close()


NEv=f_ML["ML_Nu_SupRes"].shape[0]
NEv=100
print(NEv)
s_supe=np.zeros(NEv)
s_real=np.zeros(NEv)
s_supe_ML=np.zeros(NEv)

Tot_real=np.zeros(NEv)
Tot_supe=np.zeros(NEv)
Tot_ML = np.zeros(NEv)

for ev in range(NEv):

    if ev % 10==0: print("____ EV : ", ev)

    Et_real = 0
    px_real = 0
    py_real = 0
    pz_real = 0

    Et_supe = 0
    px_supe = 0
    py_supe = 0
    pz_supe = 0

    Et_supe_ML = 0
    px_supe_ML = 0
    py_supe_ML = 0
    pz_supe_ML = 0

    for	l in range(2):

    #coord = []
    #energy = []
        En_ML = f_ML["ML_Nu_SupRes"][ev][l]
        En_real = f_input["Nu_RealRes_Target"][ev][l]
        En_supe = f_input["Nu_SupRes_Target"][ev][l]

        DZ=Z[l]-length[l]/2.

        for X in range(orig_pix):
            for Y in range(orig_pix):

                Cell=En_supe[X][Y]

                if Cell>10:
                    

                    #coord.append([X, Y])
                    #energy.append(En[X][Y])
                    
                    DX=conv_to_cm(X, supL[l]) - Ev_X_or[ev]
                    DY=conv_to_cm(Y, supL[l]) - Ev_Y_or[ev]
                    
                    r=m.sqrt(DX**2 + DY**2 + DZ**2)
                              
                    Et_supe+= Cell
                    px_supe+= DX * Cell/r
                    py_supe+= DY * Cell/r
                    pz_supe+= DZ * Cell/r

                Cell=En_ML[X][Y]
                #if Cell>30:
                #    print("Layer", l,"XY",X,Y,"Energy", Cell)
                if Cell>10:

                    
                    #coord.append([X, Y])                                                                                                                                    
                    #energy.append(En[X][Y])                                                                                                                                 

                    DX=conv_to_cm(X, supL[l]) - Ev_X_or[ev]
                    DY=conv_to_cm(Y, supL[l]) - Ev_Y_or[ev]

                    r=m.sqrt(DX**2 + DY**2 + DZ**2)

                    #if Et_supe_ML<pz_supe_ML: print(Cell,DZ * Cell/r)
                    Et_supe_ML+= Cell
                    px_supe_ML+= DX * Cell/r
                    py_supe_ML+= DY * Cell/r
                    pz_supe_ML+= DZ * Cell/r

                if X < LayerPix[l] and Y < LayerPix[l]:
                    Cell = En_real[X][Y]

                    if Cell!=0:

                        DX=conv_to_cm(X, LayerPix[l]) - Ev_X_or[ev]
                        DY=conv_to_cm(Y, LayerPix[l]) - Ev_Y_or[ev]
                        
                        r=m.sqrt(DX**2 + DY**2 + DZ**2)
                        
                        Et_real+=Cell
                        px_real+= DX * Cell/r
                        py_real+= DY * Cell/r
                        pz_real+= DZ * Cell/r

                        #coord= np.asarray(coord)
                        #energy= np.asarray(energy)

    ptot_real = np.asarray([Et_real, px_real, py_real, pz_real])
    ptot_supe = np.asarray([Et_supe, px_supe, py_supe, pz_supe])
    ptot_supe_ML = np.asarray([Et_supe_ML, px_supe_ML, py_supe_ML, pz_supe_ML])

    #print(Et_supe_ML, Et_supe)

    
    s_supe[ev]=invmass(ptot_supe)
    s_supe_ML[ev]=invmass(ptot_supe_ML)
    s_real[ev]=invmass(ptot_real)

    Tot_real[ev]=Et_real
    Tot_supe[ev]=Et_supe
    Tot_ML[ev]=Et_supe_ML


plt.hist(s_supe,color="blue", alpha = 0.7, label="Sup Truth")
plt.hist(s_supe_ML,color="lime", alpha = 0.7, label="Sup ML")
plt.hist(s_real,color="red",  alpha = 0.7, label="Real Truth")
plt.legend()
plt.savefig("plt/invmass.png")
plt.close()

plt.hist(Tot_supe,color="blue", alpha = 0.7, label="Sup Truth")
plt.hist(Tot_ML,color="lime", alpha = 0.7, label="Sup ML")
plt.hist(Tot_real,color="red",  alpha = 0.7, label="Real Truth")
plt.legend()
plt.savefig("plt/Etot.png")
plt.close()

#kmeans = KMeans(n_clusters=2, init='k-means++', max_iter=1000, n_init=100, random_state=0)
    #predictions = kmeans.fit(coord, sample_weight=energy)
    
    #ph_1 = kmeans.cluster_centers_[0]
    #ph_2 = kmeans.cluster_centers_[1]
    
    #DX_1 = conv_to_cm(ph_1[0])-conv_to_cm(Ev_X_or_pix[ev])
    #DY_1 = conv_to_cm(ph_1[1])-conv_to_cm(Ev_Y_or_pix[ev])
    #DX_2 = conv_to_cm(ph_2[0])-conv_to_cm(Ev_X_or_pix[ev])
    #DY_2 = conv_to_cm(ph_2[1])-conv_to_cm(Ev_Y_or_pix[ev])
    
   # R_1 = m.sqrt(DX_1**2+DY_1**2+DZ**2)
   # R_2 = m.sqrt(DX_2**2+DY_2**2+DZ**2)
   # 
   # E_ph_1 = np.sum(energy[np.where(predictions.labels_==1)])
   # E_ph_2 = np.sum(energy[np.where(predictions.labels_==0)])
   # 
   # pT_1 = np.asarray([E_ph_1, DX_1*E_ph_1/R_1, DY_1*E_ph_1/R_1, DZ*E_ph_1/R_1])
   # pT_2 = np.asarray([E_ph_2, DX_2*E_ph_2/R_2, DY_2*E_ph_2/R_2, DZ*E_ph_2/R_2])
   # 
   # pT_tot = pT_1+pT_2 
   # pT_pi= np.asarray([pT_tot[0],px, py, pz])

    
    #s_phot[ev]=invmass(pT_tot)
    #s_pi[ev]=invmass(pT_pi)
#print(s_phot)
#print(s_pi)


# In[21]:


#print(kmeans.cluster_centers_)
#pp=(Et,px, py, pz)
#print(invmass(pp), pp)


# In[92]:


#ph_1=kmeans.cluster_centers_[0]
#ph_2=kmeans.cluster_centers_[1]

#DX_1 = -(Ev_X_or_pix[ev]-ph_1[0])*det_size/true_pix
#DY_1 = -(Ev_Y_or_pix[ev]-ph_1[1])*det_size/true_pix
#DX_2 = -(Ev_X_or_pix[ev]-ph_2[0])*det_size/true_pix
#DY_2 = -(Ev_Y_or_pix[ev]-ph_2[1])*det_size/true_pix

#print(DX_1, DY_1)
#print(DX_2, DY_2)

#R_1=m.sqrt(DX_1**2+DY_1**2+DZ**2)
#R_2=m.sqrt(DX_2**2+DY_2**2+DZ**2)

#print(R_1, R_2)


# In[93]:


#E_ph_1=np.sum(energy[np.where(predictions.labels_==1)])
#E_ph_2=np.sum(energy[np.where(predictions.labels_==0)])
