#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import sys
import random
#import cv2
import numpy as np
import h5py
import matplotlib.pyplot as plt
import math
import uproot 
import time
from training_func import*
from matplotlib.colors import LogNorm


# In[2]:

LL = "_4L"
NEvent = 10000
PATH_FILE='/home/santilor/GPU_SupRes/PFlowNtupleFile_HOMDet_2to5GeV_Overlap_WS.root'


h_file = h5py.File('output/true_input'+str(LL)+'.h5','w')


# In[3]:


f = uproot.open(PATH_FILE)


# In[4]:


#f['EventTree'].keys()


# In[5]:


true_pix, orig_pix, det_size = 128, 64, 125.
LayerPix = np.array([64, 16, 16, 16])
supL=np.array([64, 64, 32, 32])
#LayerPix = np.array([64, 16])
#supL=np.array([64, 64])
#No_Lay=np.array([13,34])


print("Read TTree - ", NEvent)
print(len(LayerPix))
train_size, val_size = int(0.8 * NEvent), int(0.2 * NEvent)
test_size = NEvent - (train_size + val_size)
indices = list(range(NEvent))
np.random.shuffle(indices)


### --- Import CH ---###
start = time.time()
Ch_Layer_Orig = [f['EventTree'].array('cellCh_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0,int(NEvent/1000)  )] 
Ch_Layer_Orig = np.stack(Ch_Layer_Orig, axis=0)[:,:,:len(LayerPix),:,:]
Ch_Layer_Orig = Ch_Layer_Orig.reshape(NEvent, len(LayerPix), true_pix, true_pix)
end = time.time()
print(end-start, "Ch import")

# In[8]:


### --- Import NU ---###
start = time.time()
Nu_Layer_Orig = [f['EventTree'].array('cellNu_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0,int(NEvent/1000)  )] 
Nu_Layer_Orig = np.stack(Nu_Layer_Orig, axis=0)[:,:,:len(LayerPix),:,:]
Nu_Layer_Orig = Nu_Layer_Orig.reshape(NEvent, len(LayerPix), true_pix, true_pix)
end = time.time()
print(end-start, "Nu import")

# In[9]:
### --- Origin shooting point --- ###
Ev_X_or = [f['EventTree'].array('Trk_X_indx', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0, int(NEvent/1000)  )]
Ev_Y_or = [f['EventTree'].array('Trk_Y_indx', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0, int(NEvent/1000)  )]
Ev_X_or = np.stack(Ev_X_or, axis=0)
Ev_X_or = Ev_X_or.reshape(NEvent, )
Ev_Y_or = np.stack(Ev_Y_or, axis=0)
Ev_Y_or = Ev_Y_or.reshape(NEvent, )
Ev_X_or*=2
Ev_Y_or*=2

Ev_X_or=Ev_X_or[indices]
Ev_Y_or=Ev_Y_or[indices]


h_file.create_dataset('Ev_X_or', data=Ev_X_or[train_size: train_size + val_size])
h_file.create_dataset('Ev_Y_or', data=Ev_Y_or[train_size: train_size + val_size])

del Ev_X_or
del Ev_Y_or

### --- Import TRK ---###
start = time.time()
Trk_X_pos = [f['EventTree'].array('Trk_X_pos', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0, int(NEvent/1000)  )]
Trk_Y_pos = [f['EventTree'].array('Trk_Y_pos', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0, int(NEvent/1000)  )]
Trk_X_pos = np.stack(Trk_X_pos, axis=0)
Trk_X_pos = Trk_X_pos.reshape(NEvent, )
Trk_Y_pos = np.stack(Trk_Y_pos, axis=0)
Trk_Y_pos = Trk_Y_pos.reshape(NEvent, )

Trk_X_pos/=10.
Trk_Y_pos/=10.

end = time.time()
print(end-start, "TRK import")


### --- Import PT ---###
start = time.time()
Track_Energy = [f['EventTree'].array('Smeared_Ch_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(0,  int(NEvent/1000)  )]
Track_Energy = np.stack(Track_Energy, axis=0)
Track_Energy = Track_Energy.reshape(NEvent, )
end = time.time()
print(end-start, "PT import")



### --- MAKE ORIG RESOLUTION : [64,64]--- ###
start = time.time()
Ch_ori=np.asarray([ [SumPixel(Ch_Layer_Orig[ev][i], orig_pix) for i in range(len(LayerPix))] for ev in range(NEvent)])
Nu_ori=np.asarray([ [SumPixel(Nu_Layer_Orig[ev][i], orig_pix) for i in range(len(LayerPix))] for ev in range(NEvent)])
end = time.time()
print(end-start, "Make Orig Res")


# In[ ]:


del Ch_Layer_Orig
del Nu_Layer_Orig


# In[12]:


### --- SMEAR ORIG RESOLUTION : [64,64]--- ###
#start = time.time()
#Ch_ori=np.asarray([[SmearImage(Ch_ori[ev][i][:orig_pix,:orig_pix]) for i in range(2)] for ev in range(NEvent) ])
#Nu_ori=np.asarray([[SmearImage(Nu_ori[ev][i][:orig_pix,:orig_pix]) for i in range(2)] for ev in range(NEvent) ])
#end = time.time()
#print(end-start, "Smear Orig Res")
#print(Ch_ori.shape)


# In[13]:


### --- MAKE SUP RESOLUTION : [64,64]--- ###
start = time.time()
Ch_sup=np.asarray([ [SumPixel(Ch_ori[ev][i], supL[i]) for i in range(len(LayerPix))] for ev in range(NEvent)])
Nu_sup=np.asarray([ [SumPixel(Nu_ori[ev][i], supL[i]) for i in range(len(LayerPix))] for ev in range(NEvent)])
end = time.time()
print(end-start, "Make Sup Res")

del Ch_ori
del Nu_ori

### --- MAKE SUP RES TARGET NU AND CH : [64,64] --- ###
targ_ch = [np.zeros((1000,len(LayerPix),orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
targ_ch = np.stack(targ_ch, axis=0)
targ_ch = targ_ch.reshape(NEvent, len(LayerPix), orig_pix, orig_pix)
    
targ_nu = [np.zeros((1000,len(LayerPix),orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
targ_nu = np.stack(targ_nu, axis=0)
targ_nu = targ_nu.reshape(NEvent, len(LayerPix), orig_pix, orig_pix)

for ev in range(NEvent):
    for l in range(len(LayerPix)):
        targ_ch[ev][l][:supL[l],:supL[l]] = Ch_sup[ev][l][:supL[l],:supL[l]]
        targ_nu[ev][l][:supL[l],:supL[l]] = Nu_sup[ev][l][:supL[l],:supL[l]]
        #targ_nu[:,l][np.where(targ_nu[:,l]<0.)]=0.


### --- MAKE REAL RESOLUTION : [64,16] --- ###
Ch_real = [np.zeros((1000,len(LayerPix),orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Ch_real = np.stack(Ch_real, axis=0)
Ch_real = Ch_real.reshape(NEvent, len(LayerPix), orig_pix, orig_pix)

Nu_real = [np.zeros((1000,len(LayerPix),orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Nu_real = np.stack(Nu_real, axis=0)
Nu_real = Nu_real.reshape(NEvent, len(LayerPix), orig_pix, orig_pix)

start = time.time()
for ev in range(NEvent):
    for i in range(len(LayerPix)):
        Ch_real[ev][i][:LayerPix[i],:LayerPix[i]]=SumPixel(Ch_sup[ev][i], LayerPix[i]) 
        Nu_real[ev][i][:LayerPix[i],:LayerPix[i]]=SumPixel(Nu_sup[ev][i], LayerPix[i])	

end = time.time()
print(end-start, "Make Real Res")
print(Ch_real.shape)


# In[ ]:


del Ch_sup
del Nu_sup


# In[16]:


### --- MAKE INPUT LAYER REAL RES : Tot + Trk [64,16;64] as [64,64,64]--- ###

input_Layer = [np.zeros((1000,len(LayerPix)+1,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
input_Layer = np.stack(input_Layer, axis=0)
input_Layer = input_Layer.reshape(NEvent, len(LayerPix)+1, orig_pix, orig_pix)

#input_Layer[:,0:2,:,:]=Ch_real+Nu_real

for ev in range(NEvent):
    for l in range(len(LayerPix)):
        input_Layer[ev][l][:LayerPix[l],:LayerPix[l]]=Ch_real[ev][l][:LayerPix[l],:LayerPix[l]]+Nu_real[ev][l][:LayerPix[l],:LayerPix[l]]
    input_Layer[ev][-1]=MakeTrackLayer(Track_Energy[ev],Trk_X_pos[ev],Trk_Y_pos[ev])

    
#print(Ch_real[indices][train_size: train_size + val_size].shape)

Ch_real = Ch_real[indices]
Nu_real = Nu_real[indices]

val_ch_real = Ch_real[train_size: train_size + val_size]
val_nu_real = Nu_real[train_size: train_size + val_size]

h_file.create_dataset('Ch_RealRes_Target', data=val_ch_real)
h_file.create_dataset('Nu_RealRes_Target', data=val_nu_real)


del Track_Energy
del Trk_X_pos
del Trk_Y_pos


# In[ ]:

del val_ch_real
del val_nu_real

del Ch_real
del Nu_real


# In[17]:


#input shape
print("in shape",input_Layer.shape)
#target shape
print("tar shape",targ_ch.shape)

# In[19]:

#train_size, val_size = int(0.8 * NEvent), int(0.2 * NEvent)
#test_size = NEvent - (train_size + val_size)
#indices = list(range(NEvent))
#np.random.shuffle(indices)




input_Layer = input_Layer[indices]
targ_ch     = targ_ch[indices]
targ_nu     = targ_nu[indices]


train_Layer, val_Layer = input_Layer[0:train_size], input_Layer[train_size: train_size + val_size]
train_ch, val_ch       =     targ_ch[0:train_size],     targ_ch[train_size: train_size + val_size]
train_nu, val_nu       =     targ_nu[0:train_size],     targ_nu[train_size: train_size + val_size]


h_file.create_dataset('Ch_SupRes_Target', data=val_ch)
h_file.create_dataset('Nu_SupRes_Target', data=val_nu)
h_file.create_dataset('Tot_RealRes_Input', data=val_Layer)


# In[21]:


np.save("numpy_fil/train_Layer"+str(LL)+".npy",train_Layer)
np.save("numpy_fil/val_Layer"+str(LL)+".npy",val_Layer)
np.save("numpy_fil/train_ch"+str(LL)+".npy",train_ch)
np.save("numpy_fil/val_ch"+str(LL)+".npy",val_ch)
np.save("numpy_fil/train_nu"+str(LL)+".npy",train_nu)
np.save("numpy_fil/val_nu"+str(LL)+".npy",val_nu)

h_file.close()
print("End Preprocessing")



