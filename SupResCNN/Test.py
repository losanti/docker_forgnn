import os
import sys
import random
import cv2
import numpy as np
import h5py
import matplotlib.pyplot as plt
import math
import uproot 
import time

############

os.environ["CUDA_VISIBLE_DEVICES"]="0"


#max NEvent 5000
NEvent = 9000
PATH='PFlowNtupleFile_HOMDet_2to5GeV_Overlap_WS.root'
NAME = "_SupRes"
PATH_NN='model_NN'+str(NAME)+'.pt'

##########

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
from torch import tensor
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor

#############

#IMPORT FUNCTIONS

#############


### ___ SMEARING ___ ###
def SmearImage(image) :

    out_image = np.zeros(image.shape)

    NPixel = image.shape[1]

    for xbin_j in range(NPixel) :
        for ybin_k in range(NPixel) :
            if(image[xbin_j][ybin_k] > 0.) :
                out_image[xbin_j][ybin_k] = np.random.normal(image[xbin_j][ybin_k], np.sqrt(image[xbin_j][ybin_k]) * 4, 1)[0]

    return out_image


####----- REALRES AS SUPRES -----####
def MakeCron(lay, upscaling):

    if upscaling == 1:
        return lay

    else:
        return np.kron(lay, np.ones((upscaling, upscaling)) )
    

###----- LAYER OF TRACK RECONSTRUCTED IN FIRST LAYER -----###
def MakeTrackLayer(tr_e, tr_x, tr_y) :

    out_image = np.zeros( [orig_pix, orig_pix] )

    x_idx = int(  (tr_x + det_size/2.)/det_size * orig_pix  )
    y_idx = int(  (tr_y + det_size/2.)/det_size * orig_pix  )

    out_image[x_idx, y_idx] = tr_e

    return out_image


###----- SUM PIXEL FROM image_l.shape to size ----###
def SumPixel(image_l, size=64) :

    orig_pixel = image_l.shape[0]

    scale = int(orig_pixel/size)
    out_image = np.zeros( [size, size] )

    for X in range( size ) :
        for Y in range( size ) :

            val = np.sum( image_l[scale*X:scale*(X+1), scale*Y:scale*(Y+1)] )

            out_image[X:X+1, Y:Y+1] = val

    return out_image

###---- LOSS FUNCTION OF CNN ----###
def LossFunction(pred, tar, total) : 
    
    total = total[:, 0:6, :, :]
    
    loc = torch.where( total >= 0. )
    total = total[loc]
    pred = pred[loc]
    tar = tar[loc]
    

    wt_avg = torch.sum(  total.cuda() * ( pred.cuda() - tar.cuda() )* ( pred.cuda() - tar.cuda() ) )
    wt_avg = wt_avg / torch.sum( total.cuda())  

    return wt_avg


###---- EXPAND
def ExpandLayer(layer) :

    size = orig_pix
    out_image = torch.zeros( layer.shape[0], 1, size, size )
    orig_pixel = layer.shape[2]

    out_image.permute(2, 3, 1, 0)[0:orig_pixel, 0:orig_pixel, :, :] = layer.permute(2, 3, 1, 0)[0:orig_pixel, 0:orig_pixel, :, :]      

    return out_image

#############

cuda_device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu' )

###########

torch.cuda.get_device_name( torch.cuda.current_device() )

#########

###
#### IMPORT FILE AND CODE #####
###
f = uproot.open(PATH)
f['EventTree'].keys()

######

#Dimensions
true_pix, orig_pix, det_size = 128, 64, 1250.
LayerPix = np.array([64, 32, 32, 16, 16, 8])
supL=[64,64,32,16,16,8]
Lay_No=[13,34,17,14,8,14]


#CH
start = time.time()

Ch_Layer_Orig = [f['EventTree'].array('cellCh_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(90,int(NEvent/1000)+90  )] 
Ch_Layer_Orig = np.stack(Ch_Layer_Orig, axis=0)
Ch_Layer_Orig = Ch_Layer_Orig.reshape(NEvent, 6, true_pix, true_pix)

end = time.time()
print(end-start, "Ch import")

#NU
start = time.time()
Nu_Layer_Orig = [f['EventTree'].array('cellNu_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(90,int(NEvent/1000) +90 )] 
Nu_Layer_Orig = np.stack(Nu_Layer_Orig, axis=0)
Nu_Layer_Orig = Nu_Layer_Orig.reshape(NEvent, 6, true_pix, true_pix)
end = time.time()
print(end-start, "Nu import")

#TRK
Trk_X_pos = [f['EventTree'].array('Trk_X_pos', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(90, int(NEvent/1000) +90)]
Trk_Y_pos = [f['EventTree'].array('Trk_Y_pos', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(90, int(NEvent/1000) +90 )]
Trk_X_pos = np.stack(Trk_X_pos, axis=0)
Trk_X_pos = Trk_X_pos.reshape(NEvent, )
Trk_Y_pos = np.stack(Trk_Y_pos, axis=0)
Trk_Y_pos = Trk_Y_pos.reshape(NEvent, )


#PT
Track_Energy = [f['EventTree'].array('Smeared_Ch_Energy', entrystart = i*1000, entrystop = (i+1)*1000) for i in range(90,  int(NEvent/1000) +90 )]
Track_Energy = np.stack(Track_Energy, axis=0)
Track_Energy = Track_Energy.reshape(NEvent, )


# Target 
target_Layer = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
target_Layer = np.stack(target_Layer, axis=0)
target_Layer = target_Layer.reshape(NEvent, 6, orig_pix, orig_pix)

# REAL RES INPUT
input_Layer = [np.zeros((1000,7,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
input_Layer = np.stack(input_Layer, axis=0)
input_Layer = input_Layer.reshape(NEvent, 7, orig_pix, orig_pix)

# TRACK POS + PT
Track_Layer=[np.zeros((1000, orig_pix,orig_pix))  for i in range(0, int(NEvent/1000) )]
Track_Layer = np.stack(Track_Layer, axis=0)
Track_Layer = Track_Layer.reshape(NEvent, orig_pix, orig_pix)

# TARGET FRACTIONS
nu_frac=[np.zeros((1000, 6,orig_pix,orig_pix))  for i in range(0, int(NEvent/1000) )]
nu_frac = np.stack(nu_frac, axis=0)
nu_frac = nu_frac.reshape(NEvent,6, orig_pix, orig_pix)

# CH and Nu original gran 64x64 all layers
Ch_in_ori = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Ch_in_ori = np.stack(Ch_in_ori, axis=0)
Ch_in_ori = Ch_in_ori.reshape(NEvent, 6, orig_pix, orig_pix)

Nu_in_ori = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Nu_in_ori = np.stack(Nu_in_ori, axis=0)
Nu_in_ori = Nu_in_ori.reshape(NEvent, 6, orig_pix, orig_pix)

# CH and Nu original gran [64,32,32,16,16,8] all layers
Ch_in = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Ch_in = np.stack(Ch_in, axis=0)
Ch_in = Ch_in.reshape(NEvent, 6, orig_pix, orig_pix)

Nu_in = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Nu_in = np.stack(Nu_in, axis=0)
Nu_in = Nu_in.reshape(NEvent, 6, orig_pix, orig_pix)

# CH and Nu original gran [64,64,32,16,16,8] all layers
Ch_in_Sres = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Ch_in_Sres = np.stack(Ch_in_Sres, axis=0)
Ch_in_Sres = Ch_in_Sres.reshape(NEvent, 6, orig_pix, orig_pix)

Nu_in_Sres = [np.zeros((1000,6,orig_pix,orig_pix)) for i in range(0, int(NEvent/1000) )]
Nu_in_Sres = np.stack(Nu_in_Sres, axis=0)
Nu_in_Sres = Nu_in_Sres.reshape(NEvent, 6, orig_pix, orig_pix)


start = time.time()
for ev in range(NEvent):
    if ev%100==0: print("__ ev: ",ev," __")

    #make input track    
    Track_Layer[ev] = MakeTrackLayer(Track_Energy[ev],Trk_X_pos[ev],Trk_Y_pos[ev])
    input_Layer[ev][-1]=Track_Layer[ev]
    for i in range(6):
        #Make input resolution 128->64
        Ch_in_ori[ev][i][:orig_pix,:orig_pix]=SumPixel(Ch_Layer_Orig[ev][i], orig_pix)
        Nu_in_ori[ev][i][:orig_pix,:orig_pix]=SumPixel(Nu_Layer_Orig[ev][i], orig_pix)
        
        #smearing energy
        Ch_in_ori[ev][i][:orig_pix,:orig_pix]=SmearImage(Ch_in_ori[ev][i][:orig_pix,:orig_pix])
        Nu_in_ori[ev][i][:orig_pix,:orig_pix]=SmearImage(Nu_in_ori[ev][i][:orig_pix,:orig_pix])
        
        #supRES 64->SupRes
        Ch_in_Sres[ev][i][:supL[i],:supL[i]]=SumPixel(Ch_in_ori[ev][i], supL[i])
        Nu_in_Sres[ev][i][:supL[i],:supL[i]]=SumPixel(Nu_in_ori[ev][i], supL[i])

        #realRes SupRes->RealRes
        Ch_in[ev][i][:LayerPix[i],:LayerPix[i]]=SumPixel(Ch_in_Sres[ev][i][:supL[i],:supL[i]], LayerPix[i])
        Nu_in[ev][i][:LayerPix[i],:LayerPix[i]]=SumPixel(Nu_in_Sres[ev][i][:supL[i],:supL[i]], LayerPix[i])

                       
        
        #input RealRes        
        input_Layer[ev][i][:LayerPix[i],:LayerPix[i]]=Ch_in[ev][i][:LayerPix[i],:LayerPix[i]]+Nu_in[ev][i][:LayerPix[i],:LayerPix[i]]
        input_Layer[ev][i][np.where(Ch_in[ev][i][:LayerPix[i],:LayerPix[i]]+Nu_in[ev][i][:LayerPix[i],:LayerPix[i]]<=0.)]=0.
        
        #input RealRes as SupRes
        target_Layer[ev][i][:supL[i],:supL[i]]=MakeCron( input_Layer[ev][i][:LayerPix[i],:LayerPix[i]] , int(supL[i]/LayerPix[i]))

        
        
end=time.time()
print(end-start, "PreProcess")

###############

#CNN 

##############

class Upconv(nn.Module):
    def __init__(self, upscale_factor):
        super(Upconv, self).__init__()

        self.prelu = nn.LeakyReLU(negative_slope=0.1)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm2d(16)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm2d(128)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(256)
        
        self.conv1 = nn.Conv2d(1, 16,kernel_size=3,stride=1,padding=1)
        self.conv2 = nn.Conv2d(16, 32,kernel_size=3,stride=1,padding=1)
        self.conv3 = nn.Conv2d(32, 64,kernel_size=3,stride=1,padding=1) 
        self.conv4 = nn.Conv2d(64, 128,kernel_size=3,stride=1,padding=1) 
        self.conv5 = nn.Conv2d(128, 128,kernel_size=3,stride=1,padding=1)
        self.conv6 = nn.Conv2d(128, 256,kernel_size=3,stride=1,padding=1)
        #self.conv7 = nn.ConvTranspose2d(256, 1,kernel_size=4,stride=2,padding=1,bias=False)
        self.conv8 = nn.PixelShuffle(upscale_factor)
        self.convf = nn.Conv2d( int(256/upscale_factor**2), 1,kernel_size=1)

    def forward(self, x):
        x = self.bn1(self.prelu(self.conv1(x)))
        x = self.bn2(self.prelu(self.conv2(x)))
        x = self.bn3(self.prelu(self.conv3(x)))
        x = self.bn4(self.prelu(self.conv4(x)))
        x = self.bn5(self.prelu(self.conv5(x)))
        x = self.bn6(self.prelu(self.conv6(x)))
        x = self.conv8(x)
        x = self.convf(x)
        x = self.relu(x)
        
        return x


class SuperResModel(nn.Module):
    
    def __init__(self):
        super(SuperResModel, self).__init__()
        self.cnn1 = Upconv(upscale_factor = int(orig_pix/LayerPix[0]) ).float()
        self.cnn2 = Upconv(upscale_factor = int(orig_pix/LayerPix[1]) ).float()
        self.cnn3 = Upconv(upscale_factor = int(orig_pix/LayerPix[2]) ).float()
        self.cnn4 = Upconv(upscale_factor = int(orig_pix/LayerPix[3]) ).float()
        self.cnn5 = Upconv(upscale_factor = int(orig_pix/LayerPix[4]) ).float()
        self.cnn6 = Upconv(upscale_factor = int(orig_pix/LayerPix[5]) ).float()

        self.cnn_comb = nn.Conv2d(7, 6,kernel_size=1,stride=1,padding=0)
        
        self.cnn1_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
        self.cnn3_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[2]),stride=int(orig_pix/supL[2]),padding=0)
        self.cnn4_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[3]),stride=int(orig_pix/supL[3]),padding=0)
        self.cnn5_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[4]),stride=int(orig_pix/supL[4]),padding=0)
        self.cnn6_out = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[5]),stride=int(orig_pix/supL[5]),padding=0)
         
    
    def forward(self, x):
        

        out1 = self.cnn1( x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float() )
        out2 = self.cnn2( x[:,1:2, 0:LayerPix[1], 0:LayerPix[1]].float() )
        out3 = self.cnn3( x[:,2:3, 0:LayerPix[2], 0:LayerPix[2]].float() )
        out4 = self.cnn4( x[:,3:4, 0:LayerPix[3], 0:LayerPix[3]].float() )
        out5 = self.cnn5( x[:,4:5, 0:LayerPix[4], 0:LayerPix[4]].float() )
        out6 = self.cnn6( x[:,5:6, 0:LayerPix[5], 0:LayerPix[5]].float() )
        out7 = x[:, 6:7, 0:orig_pix, 0:orig_pix].float()
        
        merged_im = torch.cat(  (out1, out2, out3, out4, out5, out6, out7), dim=1 )
        
        out_int = self.cnn_comb(merged_im)
        
        out1_f = ExpandLayer(  self.cnn1_out(out_int[:, 0:1, :, :])  )
        out2_f = ExpandLayer(  self.cnn2_out(out_int[:, 1:2, :, :])  )
        out3_f = ExpandLayer(  self.cnn3_out(out_int[:, 2:3, :, :])  )
        out4_f = ExpandLayer(  self.cnn4_out(out_int[:, 3:4, :, :])  )
        out5_f = ExpandLayer(  self.cnn5_out(out_int[:, 4:5, :, :])  )
        out6_f = ExpandLayer(  self.cnn6_out(out_int[:, 5:6, :, :])  )

        merged =  torch.cat(  (out1_f, out2_f, out3_f, out4_f, out5_f, out6_f), dim=1 )
              
        return merged

    
model = SuperResModel()

model.load_state_dict(torch.load(PATH_NN))
model.to(cuda_device)
model.eval()


test_dataset = TensorDataset( Tensor(  torch.from_numpy(input_Layer).float() ) , Tensor(  torch.from_numpy(target_Layer).float() ) )
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1)

MLPred=np.zeros((NEvent,6,64,64))

i=0
for  data_in, data_in_sres  in test_loader:
    if torch.cuda.is_available():
        print(data_in.shape)
        data_in = data_in.cuda()
    print(data_in.shape)
    output=model(data_in)
    
    MLPred[i]= output.detach()[0][:6]
    MLPred[i]*= data_in_sres[0][:6].cpu().numpy()                               
    
    if i%200==0:print(i, output.detach()[0][:6].shape, data_in_sres[:6].shape )
    i+=1    
    
hf = h5py.File('ML_SupRes.h5','w')
hf.create_dataset('ML_Sres', data=MLPred)
hf.create_dataset('Charged_Sres', data=Ch_in_Sres)
hf.create_dataset('Neutral_Sres', data=Nu_in_Sres)
hf.create_dataset('Charged_Rres', data=Ch_in)
hf.create_dataset('Neutral_Rres', data=Nu_in)

hf.close()
