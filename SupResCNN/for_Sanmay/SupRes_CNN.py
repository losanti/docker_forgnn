#!/usr/bin/env python
# coding: utf-8


import os
import sys
import random
import numpy as np
import h5py
import matplotlib.pyplot as plt
import math
import uproot 
import time
from training_func import*
from matplotlib.colors import LogNorm


LL="_6L"
n_epochs = 250
## Resolution for training
RES = "_Sup"

## Energy Range 
RANGE = "2to5"

## NPy folder repository
FOLD = "/eos/user/l/losanti/sharing/"+str(RANGE)

## CNN Model Name
NAME = RES+"Res_ChNu"+LL
PATH_NN=str(FOLD)+'model_NN'+str(RES)+"Res_ChNu"+str(LL)+str(RANGE)+'.pt'


## Training Parameters: Energy Threshold (Tr), Learning rate (LR), Batch size (BS)
Tr = 0.
LR = 0.0001
BS = 100

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
from torch import tensor
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor


true_pix, orig_pix, det_size = 128, 64, 125.
LayerPix = np.array([64, 16, 32, 16, 16, 8])
    
if RES == "_Real":
    supL = np.array([64, 16, 32, 16, 16, 8])
    
if RES == "_Sup":    
    supL = np.array([64, 64, 32, 16, 16, 8])

print("Import data")

train_Layer=np.load(str(FOLD)+"/train_Layer"+str(LL)+".npy")
val_Layer=np.load(str(FOLD)+"/val_Layer"+str(LL)+".npy")

train_ch=np.load(str(FOLD)+"/train_ch"+str(RES)+str(LL)+".npy")
train_nu=np.load(str(FOLD)+"/train_nu"+str(RES)+str(LL)+".npy")
val_ch=np.load(str(FOLD)+"/val_ch"+str(RES)+str(LL)+".npy")
val_nu=np.load(str(FOLD)+"/val_nu"+str(RES)+str(LL)+".npy")

print("Numpy Imported,NEvent - ",val_Layer.shape[0]+train_Layer.shape[0])

NEvent = val_Layer.shape[0]+train_Layer.shape[0]

#ev = 52
print("Print RealRes gran",LayerPix)
print("Print SupRes gran",supL)

#for l in range(6):
#    plt.figure(figsize=(24, 10))
#
#    plt.suptitle('Layer: '+str(l+1), fontsize=20)
#
#
#    plt.subplot(241)
#    plt.title("INPUT NN")
#    plt.imshow(val_Layer[ev:ev+1][0][l][:LayerPix[l],:LayerPix[l]].T, cmap = "cividis")
#    plt.colorbar()
#
#    plt.subplot(242)
#    plt.title("NU+CH TAR")
#    plt.imshow(val_nu[ev][l][:supL[l],:supL[l]].T+val_ch[ev][l][:supL[l],:supL[l]].T, cmap="cividis")
#    plt.colorbar()
#
#    plt.subplot(245)
#    plt.title("TRACK FIRST LAYER")
#    plt.imshow(val_Layer[ev][-1].T, cmap="hot")
#    plt.colorbar()
#
#    plt.subplot(243)
#    plt.title("TARGET NU")
#    plt.imshow(val_nu[ev][l][:supL[l],:supL[l]].T, cmap="GnBu_r")
#    plt.colorbar()
#
#    plt.subplot(244)
#    plt.title("TARGET CH")
#    plt.imshow(val_ch[ev][l][:supL[l],:supL[l]].T, cmap="hot")
#    plt.colorbar()
#
#    plt.savefig("plt/CNN_Truth"+str(RES)+str(LL)+"_N_"+str(l)+".png")
#
#print("Saved Preprocess image")

os.environ["CUDA_VISIBLE_DEVICES"]="0"

cuda_device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu' )

torch.cuda.get_device_name( torch.cuda.current_device() )

###############

#CNN 

##############

class Upconv(nn.Module):
    def __init__(self, upscale_factor):
        super(Upconv, self).__init__()

        self.prelu = nn.LeakyReLU(negative_slope=0.1)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm2d(16)
        self.bn2 = nn.BatchNorm2d(32)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm2d(128)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(256)
        
        self.conv1 = nn.Conv2d(1, 16,kernel_size=3,stride=1,padding=1)
        self.conv2 = nn.Conv2d(16, 32,kernel_size=3,stride=1,padding=1)
        self.conv3 = nn.Conv2d(32, 64,kernel_size=3,stride=1,padding=1) 
        self.conv4 = nn.Conv2d(64, 128,kernel_size=3,stride=1,padding=1) 
        self.conv5 = nn.Conv2d(128, 128,kernel_size=3,stride=1,padding=1)
        self.conv6 = nn.Conv2d(128, 256,kernel_size=3,stride=1,padding=1)
        #self.conv7 = nn.ConvTranspose2d(256, 1,kernel_size=4,stride=2,padding=1,bias=False)
        self.conv8 = nn.PixelShuffle(upscale_factor)
        self.convf = nn.Conv2d( int(256/upscale_factor**2), 1,kernel_size=1)

    def forward(self, x):
        x = self.bn1(self.prelu(self.conv1(x)))
        x = self.bn2(self.prelu(self.conv2(x)))
        x = self.bn3(self.prelu(self.conv3(x)))
        x = self.bn4(self.prelu(self.conv4(x)))
        x = self.bn5(self.prelu(self.conv5(x)))
        x = self.bn6(self.prelu(self.conv6(x)))
        x = self.conv8(x)
        x = self.convf(x)
        x = self.relu(x)
        
        
        return x

class SuperResModel(nn.Module):
    
    def __init__(self):
        super(SuperResModel, self).__init__()
        self.cnn1 = Upconv(upscale_factor = int(orig_pix/LayerPix[0]) ).float()
        self.cnn2 = Upconv(upscale_factor = int(orig_pix/LayerPix[1]) ).float()
        self.cnn3 = Upconv(upscale_factor = int(orig_pix/LayerPix[2]) ).float()
        self.cnn4 = Upconv(upscale_factor = int(orig_pix/LayerPix[3]) ).float()
        self.cnn5 = Upconv(upscale_factor = int(orig_pix/LayerPix[4]) ).float()
        self.cnn6 = Upconv(upscale_factor = int(orig_pix/LayerPix[5]) ).float()
        
        self.cnn_comb = nn.Conv2d(7,12 ,kernel_size=1,stride=1,padding=0)

        
        self.cnn1_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
        self.cnn3_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[2]),stride=int(orig_pix/supL[2]),padding=0)
        self.cnn4_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[3]),stride=int(orig_pix/supL[3]),padding=0)
        self.cnn5_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[4]),stride=int(orig_pix/supL[4]),padding=0)
        self.cnn6_out_nu = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[5]),stride=int(orig_pix/supL[5]),padding=0)
        
        self.cnn1_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[0]),stride=int(orig_pix/supL[0]),padding=0)
        self.cnn2_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[1]),stride=int(orig_pix/supL[1]),padding=0)
        self.cnn3_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[2]),stride=int(orig_pix/supL[2]),padding=0)
        self.cnn4_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[3]),stride=int(orig_pix/supL[3]),padding=0)
        self.cnn5_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[4]),stride=int(orig_pix/supL[4]),padding=0)
        self.cnn6_out_ch = nn.Conv2d(1, 1,kernel_size=int(orig_pix/supL[5]),stride=int(orig_pix/supL[5]),padding=0)
                
    def forward(self, x):
        

        out1 = self.cnn1( x[:,0:1, 0:LayerPix[0], 0:LayerPix[0]].float() )
        out2 = self.cnn2( x[:,1:2, 0:LayerPix[1], 0:LayerPix[1]].float() )
        out3 = self.cnn3( x[:,2:3, 0:LayerPix[2], 0:LayerPix[2]].float() )
        out4 = self.cnn4( x[:,3:4, 0:LayerPix[3], 0:LayerPix[3]].float() )
        out5 = self.cnn5( x[:,4:5, 0:LayerPix[4], 0:LayerPix[4]].float() )
        out6 = self.cnn6( x[:,5:6, 0:LayerPix[5], 0:LayerPix[5]].float() )

        trk = x[:, 6:7, 0:orig_pix, 0:orig_pix].float()
        
        out_int=self.cnn_comb(torch.cat(  (out1, out2, out3, out4, out5, out6, trk), dim=1 )) 

        out1_f_nu = ExpandLayer( self.cnn1_out_nu( out_int[:, 0:1, :, :]) )
        out2_f_nu = ExpandLayer( self.cnn2_out_nu( out_int[:, 1:2, :, :]) )
        out3_f_nu = ExpandLayer( self.cnn3_out_nu( out_int[:, 2:3, :, :]) )
        out4_f_nu = ExpandLayer( self.cnn4_out_nu( out_int[:, 3:4, :, :]) )
        out5_f_nu = ExpandLayer( self.cnn5_out_nu( out_int[:, 4:5, :, :]) )
        out6_f_nu = ExpandLayer( self.cnn6_out_nu( out_int[:, 5:6, :, :]) )
        
        out1_f_ch = ExpandLayer( self.cnn1_out_ch( out_int[:, 6:7, :, :]) )
        out2_f_ch = ExpandLayer( self.cnn2_out_ch( out_int[:, 7:8, :, :]) )
        out3_f_ch = ExpandLayer( self.cnn3_out_ch( out_int[:, 8:9, :, :]) )
        out4_f_ch = ExpandLayer( self.cnn4_out_ch( out_int[:, 9:10, :, :]) )
        out5_f_ch = ExpandLayer( self.cnn5_out_ch( out_int[:, 10:11, :, :]) )
        out6_f_ch = ExpandLayer( self.cnn6_out_ch( out_int[:, 11:12, :, :]) )
        
        
        merged =  torch.cat(  (out1_f_nu, out2_f_nu, out3_f_nu, out4_f_nu, out5_f_nu, out6_f_nu,
                              out1_f_ch, out2_f_ch, out3_f_ch, out4_f_ch, out5_f_ch, out6_f_ch ) , dim=1)

        return merged


print("Create Model")
model = SuperResModel()
model.to(cuda_device)


print("Data Loading")
train_dataset = TensorDataset( Tensor( torch.from_numpy(train_Layer).float() ),
                               Tensor( torch.from_numpy(  train_nu ).float() ),
                               Tensor( torch.from_numpy(  train_ch ).float() )  )
valid_dataset = TensorDataset( Tensor( torch.from_numpy(  val_Layer).float() ),
                               Tensor( torch.from_numpy( val_nu    ).float() ),
                               Tensor( torch.from_numpy( val_ch    ).float() )  )


train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=BS)
valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=BS)




optimizer = optim.Adam(model.parameters(), lr=LR)
total_step = len(train_loader)
print("Num of Batches",total_step)



###---- LOSS FUNCTION OF CNN ----###
def LossFunction(pred,tar_nu ,tar_ch) : 
    pred_nu=pred[:,0:6,:,:]
    pred_ch=pred[:,6:12,:,:]
    
    loc=np.where((tar_nu.cpu()+tar_ch.cpu()>Tr))
    pred_ch=pred_ch[loc]
    pred_nu=pred_nu[loc]
    tar_ch=tar_ch.cpu()[loc]
    tar_nu=tar_nu.cpu()[loc]
    

    wt_nu=torch.sum(  ( tar_nu.cuda() - pred_nu.cuda() )*( tar_nu.cuda() - pred_nu.cuda() ) )
    wt_nu/=torch.sum(tar_nu.cuda())

    wt_ch=torch.sum(( tar_ch.cuda() - pred_ch.cuda() )*( tar_ch.cuda() - pred_ch.cuda() ))
    wt_ch/=torch.sum(tar_ch.cuda() )

    return wt_nu+wt_ch



train_loss_v, valid_loss_v = [], []

valid_loss_min = np.Inf # track change in validation loss


# In[28]:


if( len(valid_loss_v) > 0 ) : 
    valid_loss_min = np.min( np.array(valid_loss_v) )


# In[29]:

print("Start Training")
k=0
for epoch in range(1, n_epochs+1):

    
    # keep track of training and validation loss
    train_loss = 0.0
    valid_loss = 0.0
    
    ###################
    # train the model #
    ###################
    
    model.train() ## --- set the model to train mode -- ##
    for in_data, target_nu, target_ch in train_loader:
        # move tensors to GPU if CUDA is available
        if torch.cuda.is_available():
            #print(target.shape)
            
            in_data, target_nu, target_ch = in_data.cuda(), target_nu.cuda(), target_ch.cuda()
        # clear the gradients of all optimized variables
        optimizer.zero_grad()
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(in_data)
        # calculate the batch loss
        #print(target_nu.shape, output.shape)
        loss = LossFunction(output, target_nu, target_ch)
        # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()
        # perform a single optimization step (parameter update)
        optimizer.step()
        # update training loss
        train_loss += loss.item()#*data.size(0)
    
    ######################    
    # validate the model #
    ######################

    model.eval()
    for in_data, target_nu, target_ch in valid_loader:
        # move tensors to GPU if CUDA is available
        if torch.cuda.is_available():
            in_data, target_nu, target_ch = in_data.cuda(), target_nu.cuda(), target_ch.cuda()
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(in_data)
        # calculate the batch loss
        loss = LossFunction(output, target_nu, target_ch)
        # update average validation loss 
        valid_loss += loss.item()#*data.size(0)
    
    # calculate average losses
    train_loss = train_loss/len(train_loader.dataset)
    valid_loss = valid_loss/len(valid_loader.dataset)
        
        
    # print training/validation statistics 
    print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
        epoch, train_loss, valid_loss))
    
    train_loss_v.append(train_loss) 
    valid_loss_v.append(valid_loss)
    
    # save model if validation loss has decreased
    if valid_loss <= valid_loss_min:
        print('Validation loss decreased ({:.6f} --> {:.6f}).  Saving model ...'.format(
        valid_loss_min,
        valid_loss))
        torch.save(model.state_dict(), PATH_NN)
        valid_loss_min = valid_loss
        k=0
    if valid_loss > valid_loss_min:
        k+=1 
    if k==4:
        print("Validation Loss not decreasef in 4  Epochs... Stop Training")
        break
    
print("... End Training!")






#model.load_state_dict(torch.load(PATH_NN))
#model.to(cuda_device)
#model.eval()


#test_out = model(  Tensor(val_Layer[ev:ev+1]).cuda() )




#for l in range(6):
#
#
#    up = int(supL[l]/LayerPix[l])
#    loc = np.where(val_Layer[ev:ev+1][0][l][:LayerPix[l],:LayerPix[l]]>Tr)
#    suploc=Make_Sup_Loc(loc, up)
#
#    
#
#    
#    plt.figure(figsize=(24, 10))
#
#    plt.suptitle('Layer: '+str(l+1), fontsize=20)
#
#    ##INPUT NN
#    plot = np.zeros((LayerPix[l],LayerPix[l]))
#    plot[loc]=val_Layer[ev:ev+1][0][l][:LayerPix[l],:LayerPix[l]][loc]
#    
#    plt.subplot(241)
#    plt.title("INPUT NN")
#    plt.imshow(plot.T, cmap = "cividis")
#    plt.colorbar()
#
#    ##TOT TARG
#    plot = np.zeros((supL[l],supL[l]))
#    plot[suploc]=val_nu[ev][l][:supL[l],:supL[l]][suploc]+val_ch[ev][l][:supL[l],:supL[l]][suploc]   
#
#    plt.subplot(242)
#    plt.title("NU+CH TAR")
#    plt.imshow(plot.T, cmap="cividis")  
#    #plt.imshow(val_nu[ev][l][:supL[l],:supL[l]].T+val_ch[ev][l][:supL[l],:supL[l]].T, cmap="cividis")
#    plt.colorbar()
#
#    
#    ##TRACK
#    plt.subplot(245)
#    plt.title("TRACK FIRST LAYER")
#    plt.imshow(val_Layer[ev][-1].T, cmap="hot_r")
#    plt.colorbar()
#
#    
#    ##TARG NU
#    plot[suploc]=val_nu[ev][l][:supL[l],:supL[l]][suploc]
#    
#    plt.subplot(243)
#    plt.title("TARGET NU")
#    plt.imshow(plot.T, cmap="GnBu")
#    plt.colorbar()
#
#    ##TAR CH
#    plot[suploc]=val_ch[ev][l][:supL[l],:supL[l]][suploc]
#    
#    plt.subplot(244)
#    plt.title("TARGET CH")
#    plt.imshow(plot.T, cmap="hot_r")
#    plt.colorbar()
#
#    ##PRED NU
#    plot[suploc]=test_out[0][l][:supL[l],:supL[l]][suploc].detach().numpy()
#    
#    plt.subplot(247)
#    plt.title("OUTPUT NN NU")
#    plt.imshow(plot.T, cmap="GnBu")
#    plt.colorbar()
#
#    ##PRED CH
#    
#    plot[suploc]=test_out[0][l][:supL[l],:supL[l]][suploc].detach().numpy() + test_out[0][l+6][:supL[l],:supL[l]][suploc].detach().numpy()
#    
#    plt.subplot(246)
#    plt.title("OUTPUT NN TOT ")
#    plt.imshow(plot.T, cmap="cividis")
#    plt.colorbar()
#
#    plot[suploc]=test_out[0][l+6][:supL[l],:supL[l]][suploc].detach().numpy()
#    
#    plt.subplot(248)
#    plt.title("OUTPUT NN CH")
#    plt.imshow(plot.T, cmap="hot_r")
#    plt.colorbar()
#
#    plt.savefig("plt/to_s/CNN_Pred"+str(NAME)+"_N_"+str(l)+".png")
#    plt.close()


