#############

#IMPORT FUNCTIONS

#############
import numpy as np
import torch 



det_size=125.
orig_pix = 64
true_pix = 128


### ___ SMEARING ___ ###
def SmearImage(image) :

    out_image = np.zeros(image.shape)

    NPixel = image.shape[1]

    for xbin_j in range(NPixel) :
        for ybin_k in range(NPixel) :
            if(image[xbin_j][ybin_k] > 0.) :
                out_image[xbin_j][ybin_k] = np.random.normal(image[xbin_j][ybin_k], np.sqrt(image[xbin_j][ybin_k]) * 4, 1)[0]

    return out_image


####----- REALRES AS SUPRES -----####
def MakeCron(lay, upscaling):

    if upscaling == 1:
        return lay

    else:
        return np.kron(lay, np.ones((upscaling, upscaling)) )

### --- MAKE LOC FOR SUP RES IMAGE --- ###
def Make_Sup_Loc(locat, upscaling):
    newX=[]
    newY=[]
    if upscaling ==1: return locat
    
    for lX,lY in zip(locat[0], locat[1]):
        #print(lX,lY)
        for Xi in range(1,upscaling+1):
            for Yi in range(1,upscaling+1):
                
                newX.append(upscaling*(lX+1) -Xi)
                newY.append(upscaling*(lY+1) -Yi)

    loc_new=tuple([np.asarray(newX),np.asarray(newY) ])
    return loc_new    

### --- Make Z Pos --- ###
def MakePos(orx_,ory_, Z_, theta_, phi_ ):
    x_cm = orx_ + z_*np.tan(theta_)*np.cos(phi_)
    y_cm = ory_ + z_*np.tan(theta_)*np.sin(phi_)
    return x_cm, y_cm

    
###------ CONV PIX TO CM AND VICE VERSA -----###    
def conv_to_cm(pix_array, pix):
    out = (pix_array-pix/2.)*det_size/pix
    return out

def conv_to_pix(cm_array, pix):
    out = (cm_array+det_size/2.)*pix/det_size
    return int(out)


###----- LAYER OF TRACK RECONSTRUCTED IN FIRST LAYER -----###
def MakeTrackLayer(tr_e, tr_x, tr_y) :

    
    out_image = np.zeros( [orig_pix, orig_pix] )

    x_idx = conv_to_pix(tr_x,orig_pix )
    y_idx = conv_to_pix(tr_y,orig_pix )

    out_image[x_idx, y_idx] = tr_e

    return out_image


###----- SUM PIXEL FROM image_l.shape to size ----###
def SumPixel(image_l, size=64) :

    orig_pixel = image_l.shape[0]

    scale = int(orig_pixel/size)
    if scale ==1:
        return image_l
    
    out_image = np.zeros( [size, size] )

    for X in range( size ) :
        for Y in range( size ) :

            val = np.sum( image_l[scale*X:scale*(X+1), scale*Y:scale*(Y+1)] )

            out_image[X:X+1, Y:Y+1] = val

    return out_image




###---- EXPAND
def ExpandLayer(layer) :

    size = orig_pix
    out_image = torch.zeros( layer.shape[0], 1, size, size )
    orig_pixel = layer.shape[2]

    out_image.permute(2, 3, 1, 0)[0:orig_pixel, 0:orig_pixel, :, :] = layer.permute(2, 3, 1, 0)[0:orig_pixel, 0:orig_pixel, :, :]      

    return out_image
